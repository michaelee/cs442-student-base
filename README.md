# CS 442 Code Repository

This private repository is shared between you and the course instructor, and
will be used to version control and submit machine problems. All machine
problems should be added under the `mp` subdirectory in separate folders named
"01", "02", "03", etc., corresponding to the machine problem number. 

When creating your projects, be sure to uncheck the "Create Git repository"
option in Xcode. Simply add them to the appropriate folder under the cloned
repository root --- you can then use either Xcode or a command line Git client
to add and commit your changes.

After cloning this repository, you may wish to add your name  the contents of this
README file with, at minimum, your name, email address, and student ID.
